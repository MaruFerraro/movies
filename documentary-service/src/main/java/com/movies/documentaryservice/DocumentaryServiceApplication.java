package com.movies.documentaryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentaryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentaryServiceApplication.class, args);
	}

}
