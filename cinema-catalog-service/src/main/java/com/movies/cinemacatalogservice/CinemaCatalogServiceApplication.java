package com.movies.cinemacatalogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaCatalogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinemaCatalogServiceApplication.class, args);
	}

}
